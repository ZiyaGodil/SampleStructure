﻿using fierce.Helper.Base;
using fierce.Model;
using fierce.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace fierce.View.Mainpages
{
    public class FierceConversationsXaml : BaseContentPage<FierceConversationsPageViewModel> { }
    public partial class FierceConversationsPage : FierceConversationsXaml
    {
        SwipeView swipeView;
        public FierceConversationsPage()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<MessagingCenterModel>(this, "FirstLoadConversation", (sender) =>
            {
                Conversationlist.ScrollTo(0, 0, ScrollToPosition.Start, false);
            });
        }
        void SwipeView_SwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            swipeView?.Close();
            swipeView = null;
        }

        void SwipeView_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {
            swipeView = sender as SwipeView;
        }

    }
}