﻿

using Acr.UserDialogs;
using fierce.Helper.Base;
using fierce.Helper.Enums;
using fierce.Model;
using fierce.Service;
using fierce.View.Mainpages;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Threading.Tasks;
using DynamicData;
using fierce.Helper.Resource;

namespace fierce.ViewModel
{
    public class FierceConversationsPageViewModel : BaseViewModel
    {
        public ICommand NewConversation => new Command(NewConversationNavigation);
        public ICommand MycoachConversation => new Command(MycoachConversationNavigation);
        public ICommand MyTeamConversation => new Command(MyTeamConversationNavigation);
        public ICommand IndexClickedCommand => new Command<FierceConversationModel>(IndexClicked);
        //public ICommand CloseSwipeviewCommandCloseSwipeviewCommand => new Command(CloseSwipeview);
        public ICommand DeleteCommand => new Command<FierceConversationModel>(Delete);

        public ICommand LoadMoreCommand => new Command(LoadData);

        ObservableCollection<FierceConversationModel> _conversationList { get; set; } = new ObservableCollection<FierceConversationModel>();
        public ObservableCollection<FierceConversationModel> ConversationList
        {
            get => _conversationList;
            set { _conversationList = value; SetPropertyChanged(nameof(ConversationList)); }
        }

        ObservableCollection<FierceConversationModel> _allconversationList { get; set; } = new ObservableCollection<FierceConversationModel>();
        public ObservableCollection<FierceConversationModel> AllconversationList
        {
            get => _allconversationList;
            set { _allconversationList = value; SetPropertyChanged(nameof(AllconversationList)); }
        }

        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                Search();
            }
        }
        System.Drawing.Color _conversationcolor { get; set; }
        public System.Drawing.Color Conversationcolor
        {
            get { return _conversationcolor; }
            set
            {
                _conversationcolor = value;
                SetPropertyChanged(nameof(Conversationcolor));
            }
        }

        int PageIndex { get; set; } = 1;

        int _itemThreshold { get; set; } = 0;
        public int ItemThreshold
        {
            get => _itemThreshold;
            set
            {
                _itemThreshold = value;
                SetPropertyChanged(nameof(ItemThreshold));
            }
        }

        public FierceConversationsPageViewModel()
        {
            LoadData();
        }

        public async void NewConversationNavigation()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await App.Current.MainPage.Navigation.PushAsync(new ConversationsModelPage());
            IsBusy = false;
        }
        public async void MycoachConversationNavigation()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await App.Current.MainPage.Navigation.PushAsync(new AccountPage());
            IsBusy = false;
        }
        public async void MyTeamConversationNavigation()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            await App.Current.MainPage.Navigation.PushAsync(new MycoachPageXaml());
            IsBusy = false;
        }

        //public async void LoadMore()
        //{
        //    var data = await LoadData();
        //    if (data != null)
        //    {
        //        ConversationList.Add(data);
        //    }
        //}
        bool firstload { get; set; } = true;
        public async void LoadData()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {

                if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                {


                    var result = await App.FierceServiceManager.GetConversation(new GetAllConversationInputModel() { PageIndex = this.PageIndex });

                    if (result.Status && result.Data != null)
                    {
                        if (PageIndex == 1)
                        {
                            MessagingCenter.Send(new MessagingCenterModel(), "FirstLoadConversation");
                        }
                        if (result.Data.Count > 0)
                        {
                            AllconversationList.Add(result.Data);
                        }

                        if (firstload)
                        {
                            Search();
                            firstload = false;
                        }

                        if (result.Data.Count == 0 || result.Data.Count < 10)
                        {
                            ItemThreshold = -1;
                            IsBusy = false;
                            return;
                        }
                        
                        //if (firstload)
                        //{
                        //    Search();
                        //    firstload = false;
                        //}
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(AppResources.ErrorText, ServiceConfiguration.CommonErrorMessage, AppResources.OkText);
                    }

                    PageIndex += 1;

                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(AppResources.AlertText, AppResources.EnterAnswerAlertText, AppResources.OkText);
                }
            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert(AppResources.ErrorText, e.Message, AppResources.OkText);
            }
            IsBusy = false;
        }
        private async void IndexClicked(FierceConversationModel data)
        {
            if (data.IsCompleted)
            {
                var Page = new CompletedConversationPage();
                Page.ViewModel.Answer = data.Answer;
                Page.ViewModel.IsCompleted = data.IsCompleted;
                Page.ViewModel.SubConversationId = data.SubConversationId;
                Page.ViewModel.Conversation = data.Conversation;
                Page.ViewModel.Conversationcolor = data.Conversationcolor;
                Page.ViewModel.ConversationId = data.ConversationId;
                Page.ViewModel.ConversationName = data.Conversation;

                await Application.Current.MainPage.Navigation.PushAsync(Page);
                return;
            }

            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                if (data.SubConversationId == (int)SubConversation.BeachBallForm)
                {
                    var page = new BeachBallPreparationForm();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answer = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }
                else if (data.SubConversationId == (int)SubConversation.Conduct)
                {
                    var page = new ConductConversationPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }
                else if (data.SubConversationId == (int)SubConversation.Coach)
                {
                    if (data.CompletedIndex == 0)
                    {
                        var page = new MyCoachConversationPage();
                        page.ViewModel.ConversationId = data.ConversationId;
                        page.ViewModel.CurrentIndex = 1;
                        page.ViewModel.Conversation = data.Conversation;
                        page.ViewModel.Conversationcolor = data.Conversationcolor;
                        await Application.Current.MainPage.Navigation.PushAsync(page);
                    }
                    else
                    {
                        var page = new CoachOfferingPage();
                        page.ViewModel.ConversationId = data.ConversationId;
                        page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                        page.ViewModel.Conversation = data.Conversation;
                        page.ViewModel.Conversationcolor = data.Conversationcolor;
                        await Application.Current.MainPage.Navigation.PushAsync(page);
                    }
                }
                else if (data.SubConversationId == (int)SubConversation.Projectwisedecisiontree)
                {
                    var page = new DecisionTreePage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.ConversationName = data.Conversation;
                    page.ViewModel.SubConversationType = SubConversation.Projectwisedecisiontree;
                    page.ViewModel.SubConversationId = (int)SubConversation.Projectwisedecisiontree;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }
                else if (data.SubConversationId == (int)SubConversation.Taskwisedecisiontree)
                {
                    var page = new DecisionTreePage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.ConversationName = data.Conversation;
                    page.ViewModel.SubConversationType = SubConversation.Taskwisedecisiontree;
                    page.ViewModel.SubConversationId = (int)SubConversation.Taskwisedecisiontree;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.Delegation)
                {
                    var page = new DelegationConversationPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answers = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.Alignment)
                {
                    var page = new AlignmentConversationPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answer = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.Confront)
                {
                    var page = new OpeningStatementPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answers = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);

                }
                else if (data.SubConversationId == (int)SubConversation.ConfrontConduct)
                {
                    var page = new ConductConfrontConvoPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.WaypointModel)
                {

                    var page = new WaypointModelPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answers = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.AskForFeedback)
                {
                    var page = new AskFeedbackConversationPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answers = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.GiveFeedback)
                {
                    var page = new GiveFeedBackConversation();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answer = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.ReceiveFeedback)
                {
                    var page = new ReceiveFeedbackConversationPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answer = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);
                }

                else if (data.SubConversationId == (int)SubConversation.ContextResultsCycle)
                {
                    var page = new ContextResultPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Answers = data.Answer;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await Application.Current.MainPage.Navigation.PushAsync(page);

                }

                else if (data.SubConversationId == (int)SubConversation.CoachingToAccountability)
                {
                    var page = new CoachingAccountabilityPage();
                    page.ViewModel.ConversationId = data.ConversationId;
                    page.ViewModel.CurrentIndex = data.CompletedIndex + 1;
                    page.ViewModel.Conversation = data.Conversation;
                    page.ViewModel.Conversationcolor = data.Conversationcolor;
                    await ((NavigationPage)Application.Current.MainPage).Navigation.PushAsync(page);

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            IsBusy = false;
        }

        void Search()
        {

            if (string.IsNullOrEmpty(SearchText))
            {
                ConversationList = AllconversationList;
            }
            else
            {
                ConversationList = new ObservableCollection<FierceConversationModel>(AllconversationList.Where(x => x.Conversation.ToLower().Contains(SearchText.ToLower()) || x.SubConversation.ToLower().Contains(SearchText.ToLower())));
            }

        }

        async void Delete(FierceConversationModel data)
        {
            var confirmation = await Application.Current.MainPage.DisplayAlert(AppResources.AlertText, AppResources.DeleteConversationConfirmationText, AppResources.YesText, AppResources.NoText);
            if (!confirmation)
                return;
            try
            {
                using (UserDialogs.Instance.Loading(AppResources.LoadingText))
                {
                    if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                    {
                        var result = await App.FierceServiceManager.DeleteConversation(new DeleteConversationInputModel { ConversationId = data.ConversationId });
                        if (result != null && result.Status && result.Data != null)
                        {
                            await Application.Current.MainPage.DisplayAlert(AppResources.SuccessText, AppResources.ConversationDeletedSuccessText, AppResources.OkText);
                            ConversationList.Clear();
                            PageIndex = 1;
                            ItemThreshold = 1;
                            LoadData();
                            AllconversationList = ConversationList;
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert(AppResources.ErrorText, result.Message, AppResources.OkText);
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(AppResources.AlertText, AppResources.EnterAnswerAlertText, AppResources.OkText);
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
